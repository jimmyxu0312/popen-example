# popen-example

Example C++ code that uses `popen(3)` to call UNIX utility `file(1)` to determine file types for all files listed on the command line.

It also extracts the first field of the file(1) output (anything before the first comma).

Compile the code:
```
$ make
```
Run it on the files in the current directory:
```
$ ./a.out *
               a.out: ELF 64-bit LSB executable
            main.cpp: C source
              main.o: ELF 64-bit LSB relocatable
            Makefile: makefile script
           README.md: ASCII text
```
Compare it to the output of `file(1)`:
```
$ file *
main.cpp:  C source, ASCII text
main.o:    ELF 64-bit LSB relocatable, x86-64, version 1 (SYSV), not stripped
Makefile:  makefile script, ASCII text
README.md: ASCII text
```
